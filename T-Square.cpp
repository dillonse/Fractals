//
//  T-Square.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "T-Square.h"

T_Square::T_Square(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    TSquare_Polygones.push_back(polygon4(vertice(-0.75,-0.75,-10),vertice(0.75,-0.75,-10),vertice(0.75,0.75,-10),vertice(-0.75,0.75,-10)));
    name = "T-Square";
}

T_Square::~T_Square(){
    TSquare_Polygones.clear();
    TSquare_PolygonesPrint.clear();
}

void T_Square::print(){
    glColor3f(0.5,0.5,0.9);
    for (int i=0;i<TSquare_Polygones.size();i++){
        glBegin(GL_QUADS);
        polygon4 &p = TSquare_Polygones[i];
        vertice &v1 = std::get<0>(p);
        vertice &v2 = std::get<1>(p);
        vertice &v3 = std::get<2>(p);
        vertice &v4 = std::get<3>(p);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
         glEnd();
    }
    for(int i=0;i<TSquare_PolygonesPrint.size();i++){
        glBegin(GL_QUADS);
        polygon4 &p = TSquare_PolygonesPrint[i];
        vertice &v1 = std::get<0>(p);
        vertice &v2 = std::get<1>(p);
        vertice &v3 = std::get<2>(p);
        vertice &v4 = std::get<3>(p);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        glEnd();
    }
}

void T_Square::update(){
    polygon4_quadruplet pq;
    size_t last_size= TSquare_Polygones.size();
    for(int i=0;i<last_size;i++){
        polygon4&p = TSquare_Polygones[0];
        find(p,&pq);
        TSquare_Polygones.push_back(std::get<0>(pq));
        TSquare_Polygones.push_back(std::get<1>(pq));
        TSquare_Polygones.push_back(std::get<2>(pq));
        TSquare_Polygones.push_back(std::get<3>(pq));
        TSquare_Polygones.pop_front();
        TSquare_PolygonesPrint.push_back(p);
    }
}

void T_Square::find(polygon4 p,polygon4_quadruplet* pq){
    vertice&v1 = std::get<0>(p);
    vertice&v2 = std::get<1>(p);
    vertice&v3 = std::get<2>(p);
    vertice&v4 = std::get<3>(p);
    //find the midpoints
    vertice mp1 = vertice((std::get<0>(v1)+std::get<0>(v2))/2,(std ::get<1>(v1)+std::get<1>(v2))/2,(std::get<2>(v1)+std::get<2>(v2))/2);
    vertice mp2 = vertice((std::get<0>(v2)+std::get<0>(v3))/2,(std ::get<1>(v2)+std::get<1>(v3))/2,(std::get<2>(v2)+std::get<2>(v3))/2);
    vertice mp3 = vertice((std::get<0>(v3)+std::get<0>(v4))/2,(std ::get<1>(v3)+std::get<1>(v4))/2,(std::get<2>(v3)+std::get<2>(v4))/2);
    vertice mp4 = vertice((std::get<0>(v4)+std::get<0>(v1))/2,(std ::get<1>(v4)+std::get<1>(v1))/2,(std::get<2>(v4)+std::get<2>(v1))/2);
    vertice center = vertice((std::get<0>(v1)+std::get<0>(v3))/2,(std ::get<1>(v1)+std::get<1>(v3))/2,(std::get<2>(v1)+std::get<2>(v3))/2);
    //find the midpoint between each original vertex and the center//
    vertice mpd1 = vertice((std::get<0>(v1)+std::get<0>(center))/2,(std ::get<1>(v1)+std::get<1>(center))/2,(std::get<2>(v1)+std::get<2>(center))/2);
    vertice mpd2 = vertice((std::get<0>(v2)+std::get<0>(center))/2,(std ::get<1>(v2)+std::get<1>(center))/2,(std::get<2>(v2)+std::get<2>(center))/2);
    vertice mpd3 = vertice((std::get<0>(v3)+std::get<0>(center))/2,(std ::get<1>(v3)+std::get<1>(center))/2,(std::get<2>(v3)+std::get<2>(center))/2);
    vertice mpd4 = vertice((std::get<0>(v4)+std::get<0>(center))/2,(std ::get<1>(v4)+std::get<1>(center))/2,(std::get<2>(v4)+std::get<2>(center))/2);
    //find the directions to that midpoint//
    vertice d1 = vertice(std::get<0>(mpd1)-std::get<0>(center),
                         std::get<1>(mpd1)-std::get<1>(center),
                         std::get<2>(mpd1)-std::get<2>(center));
    
    vertice d2 = vertice(std::get<0>(mpd2)-std::get<0>(center),
                         std::get<1>(mpd2)-std::get<1>(center),
                         std::get<2>(mpd2)-std::get<2>(center));
    
    vertice d3 = vertice(std::get<0>(mpd3)-std::get<0>(center),
                         std::get<1>(mpd3)-std::get<1>(center),
                         std::get<2>(mpd3)-std::get<2>(center));
    
    vertice d4 = vertice(std::get<0>(mpd4)-std::get<0>(center),
                         std::get<1>(mpd4)-std::get<1>(center),
                         std::get<2>(mpd4)-std::get<2>(center));
    //find the quads//
    polygon4 p1 = polygon4(v1,mp1,center,mp4);
    polygon4 p2 = polygon4(mp1,v2,mp2,center);
    polygon4 p3 = polygon4(mp2,v3,mp3,center);
    polygon4 p4 = polygon4(mp3,v4,mp4,center);
    //move the vertices of the quads accordingly//
    //polygon1
    std::get<0>(std::get<0>(p1)) +=std::get<0>(d1);
    std::get<1>(std::get<0>(p1)) +=std::get<1>(d1);
    std::get<2>(std::get<0>(p1)) +=std::get<2>(d1);
    
    std::get<0>(std::get<1>(p1)) +=std::get<0>(d1);
    std::get<1>(std::get<1>(p1)) +=std::get<1>(d1);
    std::get<2>(std::get<1>(p1)) +=std::get<2>(d1);
    
    std::get<0>(std::get<2>(p1)) +=std::get<0>(d1);
    std::get<1>(std::get<2>(p1)) +=std::get<1>(d1);
    std::get<2>(std::get<2>(p1)) +=std::get<2>(d1);
    
    std::get<0>(std::get<3>(p1)) +=std::get<0>(d1);
    std::get<1>(std::get<3>(p1)) +=std::get<1>(d1);
    std::get<2>(std::get<3>(p1)) +=std::get<2>(d1);
    
    //polygon2
    std::get<0>(std::get<0>(p2)) +=std::get<0>(d2);
    std::get<1>(std::get<0>(p2)) +=std::get<1>(d2);
    std::get<2>(std::get<0>(p2)) +=std::get<2>(d2);
    
    std::get<0>(std::get<1>(p2)) +=std::get<0>(d2);
    std::get<1>(std::get<1>(p2)) +=std::get<1>(d2);
    std::get<2>(std::get<1>(p2)) +=std::get<2>(d2);
    
    std::get<0>(std::get<2>(p2)) +=std::get<0>(d2);
    std::get<1>(std::get<2>(p2)) +=std::get<1>(d2);
    std::get<2>(std::get<2>(p2)) +=std::get<2>(d2);
    
    std::get<0>(std::get<3>(p2)) +=std::get<0>(d2);
    std::get<1>(std::get<3>(p2)) +=std::get<1>(d2);
    std::get<2>(std::get<3>(p2)) +=std::get<2>(d2);
    
    //polygon3
    std::get<0>(std::get<0>(p3)) +=std::get<0>(d3);
    std::get<1>(std::get<0>(p3)) +=std::get<1>(d3);
    std::get<2>(std::get<0>(p3)) +=std::get<2>(d3);
    
    std::get<0>(std::get<1>(p3)) +=std::get<0>(d3);
    std::get<1>(std::get<1>(p3)) +=std::get<1>(d3);
    std::get<2>(std::get<1>(p3)) +=std::get<2>(d3);
    
    std::get<0>(std::get<2>(p3)) +=std::get<0>(d3);
    std::get<1>(std::get<2>(p3)) +=std::get<1>(d3);
    std::get<2>(std::get<2>(p3)) +=std::get<2>(d3);
    
    std::get<0>(std::get<3>(p3)) +=std::get<0>(d3);
    std::get<1>(std::get<3>(p3)) +=std::get<1>(d3);
    std::get<2>(std::get<3>(p3)) +=std::get<2>(d3);
    
    //polygon4
    std::get<0>(std::get<0>(p4)) +=std::get<0>(d4);
    std::get<1>(std::get<0>(p4)) +=std::get<1>(d4);
    std::get<2>(std::get<0>(p4)) +=std::get<2>(d4);
    
    std::get<0>(std::get<1>(p4)) +=std::get<0>(d4);
    std::get<1>(std::get<1>(p4)) +=std::get<1>(d4);
    std::get<2>(std::get<1>(p4)) +=std::get<2>(d4);
    
    std::get<0>(std::get<2>(p4)) +=std::get<0>(d4);
    std::get<1>(std::get<2>(p4)) +=std::get<1>(d4);
    std::get<2>(std::get<2>(p4)) +=std::get<2>(d4);
    
    std::get<0>(std::get<3>(p4)) +=std::get<0>(d4);
    std::get<1>(std::get<3>(p4)) +=std::get<1>(d4);
    std::get<2>(std::get<3>(p4)) +=std::get<2>(d4);
    
    
    
    *pq = polygon4_quadruplet(p1,p2,p3,p4);
}






