// The OpenGL libraries, make sure to include the GLUT and OpenGL frameworks
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <deque>
#include <iostream>
#include <tuple>
#include <math.h>

#include "SierpinskiTriangle.h"
#include "KochSnowflake.h"
#include "DragonCurve.h"
#include "T-Square.h"
#include "SpaceFillingCurve.h"
#include "SierpinskiCarpet.h"
#include "BarnsleyFern.h"
#include "CantorSet.h"
#include "MengerSponge.h"
#include "SierpinskiPyramid.h"

IFS *ifs;

void init() // Called before main loop to set up the program
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
}

// Called at the start of the program, after a glutPostRedisplay() and during idle
// to display a frame
void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    ifs->animate();
    glutSwapBuffers();
    //glEnable(GL_PROGRAM_POINT_SIZE_EXT);
}

// Called every time a window is resized to resize the projection matrix
void reshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-0.1, 0.1, -float(h)/(10.0*float(w)), float(h)/(10.0*float(w)), 0.5, 1000.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void enable_ifs(int i){
    delete ifs;
    switch (i) {
        case 0:
            ifs = new Sierpinski(0,0);
            break;
        case 1:
            ifs = new Koch(0,0);
            break;
        case 2:
            ifs = new Dragon(0,0);
            break;
        case 3:
            ifs = new T_Square(0,0);
            break;
        case 4:
            ifs = new Hilbert(0,0);
            break;
        case 5:
            ifs = new SierpinskiCarpet(0,0);
            break;
        case 6:
            ifs = new Barnsley(0,0);
            break;
        case 7:
            ifs = new Cantor(0,0);
            break;
        case 8:
            ifs= new Menger(0,1);
            break;
        case 9:
            ifs = new SierpinskiPyramid(0,1);
            break;
        default:
            ;
            ifs = new Sierpinski(0,0);
            break;
    }
}

void keyInput(unsigned char key,int x,int y){
    static int ifs_index;
    switch (key) {
        case 'd'://next ifs//
            ifs_index++;
            if(ifs_index>9)ifs_index=0;
            std::cout<<"ifs index:"<<ifs_index<<std::endl;
            enable_ifs(ifs_index);
            break;
        case 'a'://previews ifs
            ifs_index--;
            if(ifs_index<0)ifs_index=9;
            enable_ifs(ifs_index);
            break;
        case 'w'://more it//
            ifs->increment_iterations();
            break;
        default:
            break;
    }
}


int main(int argc, char **argv){
    glutInit(&argc, argv); // Initializes glut
    
    // Sets up a double buffer with RGBA components and a depth component
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    
    // Sets the window size to 512*512 square pixels
    glutInitWindowSize(1024, 1024);
    
    // Sets the window position to the upper left
    glutInitWindowPosition(0, 0);
    
    // Creates a window using internal glut functionality
    glutCreateWindow("Iterated Function Systems");
    
    // passes reshape and display functions to the OpenGL machine for callback
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutIdleFunc(display);
    glutKeyboardFunc(keyInput);
    
    init();
    ifs = new SierpinskiPyramid(0,1);
    // Starts the program.
    glutMainLoop();
    return 0;
}