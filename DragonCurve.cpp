//
//  DragonCurve.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "DragonCurve.h"


Dragon::Dragon(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Dragon_Lines.push_back(line(vertice(-0.5,-0.5,-10.0),vertice(0.5,0.5,-10.0)));
    name = "Dragon Curve";
}

Dragon::~Dragon(){
    Dragon_Lines.clear();
}

void Dragon::print(){
    glBegin(GL_LINES);
    for(int i=0;i<Dragon_Lines.size();i++){
        line& l = Dragon_Lines[i];
        vertice& v1 = std::get<0>(l);
        vertice& v2 = std::get<1>(l);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
    }
    glEnd();
}

void Dragon::update(){
    line_tuple lt;
    size_t last_size= Dragon_Lines.size();
    for(int i=0;i<last_size;i++){
        line& l = Dragon_Lines[0];
        find(l,&lt);
        Dragon_Lines.push_back(std::get<0>(lt));
        Dragon_Lines.push_back(std::get<1>(lt));
        Dragon_Lines.pop_front();
    }
}

void Dragon::find(line l,line_tuple* lt){
    vertice& v1 = std::get<0>(l);
    vertice& v2 = std::get<1>(l);
    //find the middle point//
    vertice mp1 = vertice((std::get<0>(v2)+std::get<0>(v1))/2,
                          (std::get<1>(v2)+std::get<1>(v1))/2,
                          (std::get<2>(v2)+std::get<2>(v1))/2);
    vertice direction = vertice(std::get<0>(v2)-std::get<0>(v1),
                                std::get<1>(v2)-std::get<1>(v1),
                                std::get<2>(v2)-std::get<2>(v1));
  //float length = powf(std::get<0>(v1)-std::get<0>(v2),2)+powf(std::get<1>(v1)-std::get<1>(v2),2);
    std::get<0>(direction)/=2;
    std::get<1>(direction)/=2;
    std::get<0>(mp1)+=std::get<1>(direction);
    std::get<1>(mp1)-=std::get<0>(direction);
    *lt = line_tuple(line(v1,mp1),line(v2,mp1));

}