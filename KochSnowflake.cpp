//
//  KochSnowflake.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "KochSnowflake.h"

Koch::Koch(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    glColor3f(0.5,0.5,0.5);
    KockFlake_Lines.push_back(line(vertice(0.0,1,-10),vertice(-1.0,-0.7,-10)));
    KockFlake_Lines.push_back(line(vertice(-1.0,-0.7,-10),vertice(1.0,-0.7,-10)));
    KockFlake_Lines.push_back(line(vertice(1.0,-0.7,-10),vertice(0.0,1,-10)));
    name = "Koch Snowflake";
}

Koch::~Koch(){
    KockFlake_Lines.clear();
}


void Koch::print(){
    glBegin(GL_LINES);
    for(int i=0;i<KockFlake_Lines.size();i++){
        line& l = KockFlake_Lines[i];
        vertice& v1 = std::get<0>(l);
        vertice& v2 = std::get<1>(l);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
    }
    glEnd();
}

void Koch::find(line l,line_quadruplet* lq){
    vertice& v1 = std::get<0>(l);
    vertice& v2 = std::get<1>(l);
    vertice mp1 = vertice(std::get<0>(v1)+(std::get<0>(v2)-std::get<0>(v1))/3,
                          std::get<1>(v1)+(std::get<1>(v2)-std::get<1>(v1))/3,
                          std::get<2>(v1)+(std::get<2>(v2)-std::get<2>(v1))/3);
    vertice mp2 = vertice(std::get<0>(v2)+(std::get<0>(v1)-std::get<0>(v2))/3,
                          std::get<1>(v2)+(std::get<1>(v1)-std::get<1>(v2))/3,
                          std::get<2>(v2)+(std::get<2>(v1)-std::get<2>(v2))/3);
    vertice mp3 = vertice((std::get<0>(mp1)+std::get<0>(mp2))/2,
                          (std::get<1>(mp1)+std::get<1>(mp2))/2,
                          (std::get<2>(mp1)+std::get<2>(mp2))/2);
    vertice direction = vertice(std::get<0>(mp3)-std::get<0>(v1),
                                std::get<1>(mp3)-std::get<1>(v1),
                                std::get<2>(mp3)-std::get<2>(v1));
   // float length = sqrtf(powf(std::get<0>(mp1)-std::get<0>(mp2),2)+powf(std::get<1>(mp1)-std::get<1>(mp2),2));
    std::get<0>(mp3)+=std::get<1>(direction)/2;
    std::get<1>(mp3)-=std::get<0>(direction)/2;
    *lq = line_quadruplet(line(v1,mp1),line(mp1,mp3),line(mp3,mp2),line(mp2,v2));
    
}

void Koch::update(){
    line_quadruplet lq;
    size_t last_size=KockFlake_Lines.size();
    for(int i=0;i<last_size;i++){
        line& l = KockFlake_Lines[0];
        find(l,&lq);
        KockFlake_Lines.push_back(std::get<0>(lq));
        KockFlake_Lines.push_back(std::get<1>(lq));
        KockFlake_Lines.push_back(std::get<2>(lq));
        KockFlake_Lines.push_back(std::get<3>(lq));
        KockFlake_Lines.pop_front();
    }
    
}