//
//  DragonCurve.h
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__DragonCurve__
#define __Fractals__DragonCurve__

#include <deque>
#include <math.h>
#include "IFS.h"

#include "vertice.h"

typedef std::tuple<vertice,vertice> line;
typedef std::tuple<line,line> line_tuple;

class Dragon:public IFS{
    std::deque<line> Dragon_Lines;
public:
    Dragon(int iteration_limit=5,float time_limit=2.0);
    ~Dragon();
private:
    void print();
    void update();
    void find(line l,line_tuple* lq);
};

#endif /* defined(__Fractals__DragonCurve__) */
