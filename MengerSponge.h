//
//  MengerSponge.h
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__MengerSponge__
#define __Fractals__MengerSponge__

#include "IFS.h"

#include <vector>
#include <tuple>
#include <deque>

#include "vertice.h"

typedef std::tuple<vertice,vertice,vertice,vertice,vertice,vertice,vertice,vertice> cube;
typedef std::tuple<cube,cube,cube,cube,cube,cube,cube,cube,cube,cube,
cube,cube,cube,cube,cube,cube,cube,cube,cube,cube> cube_twantuple;


class Menger:public IFS{
    std::deque<cube> Menger_Cubes;
    vertice center;
public:
    Menger(int iteration_limit=5,float time_limit=2.0);
    ~Menger();
private:
    void print();
    void update();
    void find(cube& p,cube_twantuple* ct);
};

#endif /* defined(__Fractals__MengerSponge__) */
