//
//  CantorSet.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "CantorSet.h"

Cantor::Cantor(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Cantor_Lines.push_back(line(vertice(-1,0,-5),vertice(1,0,-5)));
    name="Cantor Set";
}

Cantor::~Cantor(){
    Cantor_Lines.clear();
}

void Cantor::print(){
    glBegin(GL_LINES);
    for(int i=0;i<Cantor_Lines.size();i++){
        line& l = Cantor_Lines[i];
        vertice&v1=std::get<0>(l);
        vertice&v2=std::get<1>(l);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
    }
    glEnd();
}

void Cantor::update(){
    size_t last_size = Cantor_Lines.size();
    line_tuple lt;
    for(int i=0;i<last_size;i++){
        line& l=Cantor_Lines[0];
        find(l,&lt);
        Cantor_Lines.push_back(std::get<0>(lt));
        Cantor_Lines.push_back(std::get<1>(lt));
                                                              
        Cantor_Lines.pop_front();
    }
}

void Cantor::find(line &l,line_tuple *lt){
    vertice &v1= std::get<0>(l);
    vertice &v2= std::get<1>(l);
    
    //find the 1/3 midpoints//
    vertice mp1 = vertice(std::get<0>(v1)+(std::get<0>(v2)-std::get<0>(v1))/3,
                  std::get<1>(v1)+(std::get<1>(v2)-std::get<1>(v1))/3,
                  std::get<2>(v1)+(std::get<2>(v2)-std::get<2>(v1))/3);
    vertice mp2 = vertice(std::get<0>(v2)+(std::get<0>(v1)-std::get<0>(v2))/3,
                          std::get<1>(v2)+(std::get<1>(v1)-std::get<1>(v2))/3,
                          std::get<2>(v2)+(std::get<2>(v1)-std::get<2>(v2))/3);
    *lt = line_tuple(line(v1,mp1),line(mp2,v2));
}
