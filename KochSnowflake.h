//
//  KochSnowflake.h
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__KochSnowflake__
#define __Fractals__KochSnowflake__
#include <deque>
#include <math.h>
#include "IFS.h"

#include "vertice.h"

typedef std::tuple<vertice,vertice> line;
typedef std::tuple<line,line,line,line> line_quadruplet;

class Koch:public IFS{
    std::deque<line> KockFlake_Lines;
public:
    Koch(int iteration_limit=5,float time_limit=2.0);
    ~Koch();
private:
    void print();
    void update();
    void find(line l,line_quadruplet* lq);
};

#endif /* defined(__Fractals__KochSnowflake__) */
