//
//  SierpinskiPyramid.h
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__SierpinskiPyramid__
#define __Fractals__SierpinskiPyramid__

#include "IFS.h"
#include <deque>
#include <vector>
#include <math.h>

#include "vertice.h"

typedef std::tuple<vertice,vertice,vertice,vertice> pyramid;
typedef std::tuple<pyramid,pyramid,pyramid,pyramid> pyramid_quadruple;


class SierpinskiPyramid:public IFS{
    std::deque<pyramid> Sierpinski_Pyramids;
    vertice center;
public:
    SierpinskiPyramid(int iteration_limit=5,float time_limit=2.0);
    ~SierpinskiPyramid();
private:
    void print();
    void update();
    void find(pyramid& p,pyramid_quadruple* ct);
};


#endif /* defined(__Fractals__SierpinskiPyramid__) */
