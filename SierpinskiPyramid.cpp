//
//  SierpinskiPyramid.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "SierpinskiPyramid.h"






pyramid operator+(pyramid p,vertice v){
    return  pyramid(std::get<0>(p)+v,std::get<1>(p)+v,std::get<2>(p)+v,std::get<3>(p)+v);
}

pyramid operator-(pyramid p,vertice v){
    return  pyramid(std::get<0>(p)-v,std::get<1>(p)-v,std::get<2>(p)-v,std::get<3>(p)-v);
}



SierpinskiPyramid::SierpinskiPyramid(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Sierpinski_Pyramids.push_back(pyramid(vertice(-1,0,-11),vertice(1,0,-11),vertice(0,0,-9.268),
                                          vertice(0,1.732,-9.268-1.732/2)));
    center = vertice(0.5,0.5,-10);
    glColor3f(0.3,0.5,0.2);
    name = "Sierpinski Pyramid";
}

SierpinskiPyramid::~SierpinskiPyramid(){
    Sierpinski_Pyramids.clear();
}

void SierpinskiPyramid::print(){
    for(int i=0;i<Sierpinski_Pyramids.size();i++){
        pyramid&p=Sierpinski_Pyramids[i];
        vertice&v1=std::get<0>(p);
        vertice&v2=std::get<1>(p);
        vertice&v3=std::get<2>(p);
        vertice&v4=std::get<3>(p);
        //base//
        glPushMatrix();
        glTranslatef(std::get<0>(center),std::get<1>(center),std::get<2>(center));
        glRotatef((timer/time_limit)*360,1,1,0);
        glTranslatef(-std::get<0>(center),-std::get<1>(center),-std::get<2>(center));
        glBegin(GL_TRIANGLES);
        glColor3f(1,0,0);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        //back//
        glColor3f(0,1,0);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        //right//
        glColor3f(0,0,1);
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        //left//
        glColor3f(1,1,1);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        glEnd();
        glPopMatrix();
    }
}


void SierpinskiPyramid::update(){
    pyramid_quadruple pt;
    size_t last_size=Sierpinski_Pyramids.size();
    for (int i=0;i<last_size;i++){
        pyramid& p = Sierpinski_Pyramids[0];
        find(p,&pt);
        Sierpinski_Pyramids.push_back(std::get<0>(pt));
        Sierpinski_Pyramids.push_back(std::get<1>(pt));
        Sierpinski_Pyramids.push_back(std::get<2>(pt));
        Sierpinski_Pyramids.push_back(std::get<3>(pt));
        Sierpinski_Pyramids.pop_front();
    }
}

void SierpinskiPyramid::find(pyramid& p, pyramid_quadruple* pt){
    vertice &v1=std::get<0>(p);
    vertice &v2=std::get<1>(p);
    vertice &v3=std::get<2>(p);
    vertice &v4=std::get<3>(p);
    
    //get offsets//
    vertice offset1 = (v1-v2)/2;
    vertice offset2 = (v3-v2)/2;
    vertice offset3 = (v4-v3)/2;

    //find the first smaller pyramid//
    pyramid p1 = pyramid(v1,(v2+v1)/2,(v3+v1)/2,(v4+v1)/2);
    pyramid p2 = p1-offset1;
    pyramid p3 = p2+offset2;
    pyramid p4 = p3+offset3;
    *pt= pyramid_quadruple(p1,p2,p3,p4);
}










