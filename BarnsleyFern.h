//
//  BarnsleyFern.h
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__BarnsleyFern__
#define __Fractals__BarnsleyFern__

#include <vector>
#include <math.h>
#include "IFS.h"

#include "vertice.h"

class Barnsley:public IFS{
    std::vector<vertice> Barnsley_Points;
public:
    Barnsley(int iteration_limit=5,float time_limit=2.0);
    ~Barnsley();
private:
    void print();
    void update();
    void find(vertice& vi,vertice* vo);
};

#endif /* defined(__Fractals__BarnsleyFern__) */
