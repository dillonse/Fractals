//
//  vertice.h
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__vertice__
#define __Fractals__vertice__

#include <tuple>
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

typedef std::tuple<GLfloat,GLfloat,GLfloat> vertice;
vertice operator+(vertice v1,vertice v2);
vertice operator-(vertice v1,vertice v2);
vertice operator/(vertice v1,int i);
vertice operator*(vertice v1,int i);

#endif /* defined(__Fractals__vertice__) */
