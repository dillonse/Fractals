//
//  SpaceFillingCurve.h
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__SpaceFillingCurve__
#define __Fractals__SpaceFillingCurve__

#include <deque>
#include <vector>
#include <math.h>
#include "IFS.h"

#include "vertice.h"

typedef std::tuple<vertice,vertice> line;
typedef std::tuple<vertice,vertice,vertice,vertice> pi;
typedef std::tuple<pi,pi,pi,pi> line_pi;

class Hilbert:public IFS{
    std::deque<pi> Hilbert_pies;
public:
    Hilbert(int iteration_limit=5,float time_limit=2.0);
    ~Hilbert();
private:
    void print();
    void update();
    void find(pi p,line_pi* gp);
};

#endif /* defined(__Fractals__SpaceFillingCurve__) */
