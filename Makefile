# Linux (default)
  EXE = assignment1
  LDFLAGS = -lGL -lGLU -lglut
  
# Windows (cygwin)
  ifeq "$(OS)" "Windows_NT"
  EXE = assignment1.exe
  LDFLAGS = -lopengl32 -lglu32 -lglut32
  endif

# OS X
ifeq "$(OSTYPE)" "darwin"
LDFLAGS = -framework Carbon -framework OpenGL -framework GLUT
endif

SOURCES=main.cpp BarnsleyFern.cpp CantorSet.cpp DragonCurve.cpp IFS.cpp KochSnowflake.cpp MengerSponge.cpp SierpinskiCarpet.cpp SierpinskiPyramid.cpp SierpinskiTriangle.cpp SpaceFillingCurve.cpp T-Square.cpp vertice.cpp

$(EXE) : main.cpp
	gcc -o $@ $(SOURCES) $(CFLAGS) $(LDFLAGS) -std=c++11
