//
//  SpaceFillingCurve.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "SpaceFillingCurve.h"


Hilbert::Hilbert(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Hilbert_pies.push_back(pi(vertice(-0.5,-0.5,-10),vertice(-0.5,0.5,-10),vertice(0.5,0.5,-10),vertice(0.5,-0.5,-10)));
    name="Hilbert Curve";
}

Hilbert::~Hilbert(){
    Hilbert_pies.clear();
}

void Hilbert::print(){
    vertice lastv;
        glBegin(GL_LINE_STRIP);
        pi&p = Hilbert_pies[0];
        vertice&v1 = std::get<0>(p);
        vertice&v2 = std::get<1>(p);
        vertice&v3 = std::get<2>(p);
        vertice&v4 = std::get<3>(p);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        glEnd();
        std::get<0>(lastv)=std::get<0>(v4);
        std::get<1>(lastv)=std::get<1>(v4);
        std::get<2>(lastv)=std::get<2>(v4);
    for(int i=1;i<Hilbert_pies.size();i++){
        glBegin(GL_LINE_STRIP);
        pi&p = Hilbert_pies[i];
        vertice&v1 = std::get<0>(p);
        vertice&v2 = std::get<1>(p);
        vertice&v3 = std::get<2>(p);
        vertice&v4 = std::get<3>(p);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        glEnd();
        glBegin(GL_LINE_STRIP);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(lastv),std::get<1>(lastv),std::get<2>(lastv));
        glEnd();
        std::get<0>(lastv)=std::get<0>(v4);
        std::get<1>(lastv)=std::get<1>(v4);
        std::get<2>(lastv)=std::get<2>(v4);
    }
   
}

void Hilbert::update(){
    //scale the points//
    line_pi gp;
    size_t last_size=Hilbert_pies.size();
    for(int i=0;i<last_size;i++){
        pi& p = Hilbert_pies[0];
        find(p,&gp);
        Hilbert_pies.push_back(std::get<0>(gp));
        Hilbert_pies.push_back(std::get<1>(gp));
        Hilbert_pies.push_back(std::get<2>(gp));
        Hilbert_pies.push_back(std::get<3>(gp));
        Hilbert_pies.pop_front();
    }
    
}

void Hilbert::find(pi p,line_pi* gp){
    //scale the points//
    vertice&v1 = std::get<0>(p);
    vertice&v2 = std::get<1>(p);
    vertice&v3 = std::get<2>(p);
    vertice&v4 = std::get<3>(p);
    //scale up the pi//
    vertice offset1 = vertice((std::get<0>(v2)-std::get<0>(v1))/4,
                              (std::get<1>(v2)-std::get<1>(v1))/4,
                              (std::get<2>(v2)-std::get<2>(v1))/4);
    vertice offset2 = vertice((std::get<0>(v3)-std::get<0>(v2))/4,
                              (std::get<1>(v3)-std::get<1>(v2))/4,
                              (std::get<2>(v4)-std::get<2>(v2))/4);
    
    std::get<0>(v1)-=std::get<0>(offset2);
    std::get<1>(v1)-=std::get<1>(offset2);
   // std::get<2>(v1)-=std::get<2>(offset2);
    
    std::get<0>(v1)-=std::get<0>(offset1);
    std::get<1>(v1)-=std::get<1>(offset1);
    // std::get<2>(v1)-=std::get<2>(offset2);
    

    std::get<0>(v2)+=std::get<0>(offset1);
    std::get<1>(v2)+=std::get<1>(offset1);
    //std::get<2>(v2)-=std::get<2>(offset1);
    
    std::get<0>(v2)-=std::get<0>(offset2);
    std::get<1>(v2)-=std::get<1>(offset2);
   // std::get<2>(v2)+=std::get<2>(offset2);
    
    std::get<0>(v3)+=std::get<0>(offset1);
    std::get<1>(v3)+=std::get<1>(offset1);
   // std::get<2>(v3)+=std::get<2>(offset2);
    
    std::get<0>(v3)+=std::get<0>(offset2);
    std::get<1>(v3)+=std::get<1>(offset2);
    
    std::get<0>(v4)+=std::get<0>(offset2);
    std::get<1>(v4)+=std::get<1>(offset2);
    // std::get<2>(v1)-=std::get<2>(offset2);
    
    std::get<0>(v4)-=std::get<0>(offset1);
    std::get<1>(v4)-=std::get<1>(offset1);
    
    //find the 1/3 distance points for each side of the pi//
    //v1-v2
    vertice mp11 = vertice(std::get<0>(v1)+(std::get<0>(v2)-std::get<0>(v1))/3,
                          std::get<1>(v1)+(std::get<1>(v2)-std::get<1>(v1))/3,
                          std::get<2>(v1)+(std::get<2>(v2)-std::get<2>(v1))/3);
    
    vertice mp12 = vertice(std::get<0>(v2)+(std::get<0>(v1)-std::get<0>(v2))/3,
                           std::get<1>(v2)+(std::get<1>(v1)-std::get<1>(v2))/3,
                           std::get<2>(v2)+(std::get<2>(v1)-std::get<2>(v2))/3);
    //v2-v3
    vertice mp21 = vertice(std::get<0>(v2)+(std::get<0>(v3)-std::get<0>(v2))/3,
                           std::get<1>(v2)+(std::get<1>(v3)-std::get<1>(v2))/3,
                           std::get<2>(v2)+(std::get<2>(v3)-std::get<2>(v2))/3);
    
    vertice mp22 = vertice(std::get<0>(v3)+(std::get<0>(v2)-std::get<0>(v3))/3,
                           std::get<1>(v3)+(std::get<1>(v2)-std::get<1>(v3))/3,
                           std::get<2>(v3)+(std::get<2>(v2)-std::get<2>(v3))/3);
    //v3-v4//
    vertice mp31 = vertice(std::get<0>(v3)+(std::get<0>(v4)-std::get<0>(v3))/3,
                           std::get<1>(v3)+(std::get<1>(v4)-std::get<1>(v3))/3,
                           std::get<2>(v3)+(std::get<2>(v4)-std::get<2>(v3))/3);
    
    vertice mp32 = vertice(std::get<0>(v4)+(std::get<0>(v3)-std::get<0>(v4))/3,
                           std::get<1>(v4)+(std::get<1>(v3)-std::get<1>(v4))/3,
                           std::get<2>(v4)+(std::get<2>(v3)-std::get<2>(v4))/3);
    //find the vertical distance and the horizontal distance//
    vertice d1 = vertice(std::get<0>(mp11)-std::get<0>(mp12),std::get<1>(mp11)-std::get<1>(mp12),std::get<2>(mp11)-std::get<2>(mp12));
    vertice d2 = vertice(std::get<0>(mp21)-std::get<0>(mp22),std::get<1>(mp21)-std::get<1>(mp22),std::get<2>(mp21)-std::get<2>(mp22));
    
    vertice oldv1=vertice(v1);
    vertice oldmp11 =vertice(mp11);
    vertice oldmp21 = vertice(mp21);
    vertice oldmp22 = vertice(mp22);
    vertice oldmp32 = vertice(mp32);
    vertice oldv4 = vertice(v4);
    //move the middle points//
    std::get<0>(mp11)-=std::get<0>(d2);
    std::get<1>(mp11)-=std::get<1>(d2);
    std::get<2>(mp11)-=std::get<2>(d2);
    
    std::get<0>(v1)-=std::get<0>(d2);
    std::get<1>(v1)-=std::get<1>(d2);
    std::get<2>(v1)-=std::get<2>(d2);
    
    std::get<0>(mp21)+=std::get<0>(d1);
    std::get<1>(mp21)+=std::get<1>(d1);
    std::get<2>(mp21)+=std::get<2>(d1);
    
    std::get<0>(mp22)+=std::get<0>(d1);
    std::get<1>(mp22)+=std::get<1>(d1);
    std::get<2>(mp22)+=std::get<2>(d1);
    
    std::get<0>(mp32)+=std::get<0>(d2);
    std::get<1>(mp32)+=std::get<1>(d2);
    std::get<2>(mp32)+=std::get<2>(d2);
    
    std::get<0>(v4)+=std::get<0>(d2);
    std::get<1>(v4)+=std::get<1>(d2);
    std::get<2>(v4)+=std::get<2>(d2);
    
    *gp = line_pi(pi(oldv1,v1,mp11,oldmp11),pi(mp12,v2,oldmp21,mp21),pi(mp22,oldmp22,v3,mp31),pi(oldmp32,mp32,v4,oldv4));
}

