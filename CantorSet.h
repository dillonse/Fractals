//
//  CantorSet.h
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__CantorSet__
#define __Fractals__CantorSet__

#include <deque>
#include <vector>
#include <math.h>
#include "IFS.h"

#include "vertice.h"

typedef std::tuple<vertice,vertice> line;
typedef std::tuple<line,line> line_tuple;

class Cantor:public IFS{
    std::deque<line> Cantor_Lines;
public:
    Cantor(int iteration_limit=5,float time_limit=2.0);
    ~Cantor();
private:
    void print();
    void update();
    void find(line &l,line_tuple* lt);
};

#endif /* defined(__Fractals__CantorSet__) */
