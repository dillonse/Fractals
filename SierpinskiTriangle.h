//
//  SierpinskiTriangle.h
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__SierpinskiTriangle__
#define __Fractals__SierpinskiTriangle__
#include <deque>
#include "IFS.h"

#include "vertice.h"


typedef std::tuple<vertice,vertice,vertice> triangle;
typedef std::tuple<triangle,triangle,triangle> triangle_triplet;

class Sierpinski:public IFS{
    std::deque<triangle> Sierpinski_Triangles;
public:
    Sierpinski(int iteration_limit=5,float time_limit=2.0);
    ~Sierpinski();
private:
    void print();
    void update();
    void find(triangle t,triangle_triplet* tt);
};

#endif /* defined(__Fractals__SierpinskiTriangle__) */
