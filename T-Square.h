//
//  T-Square.h
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__T_Square__
#define __Fractals__T_Square__

#include <deque>
#include <vector>
#include <math.h>
#include "IFS.h"

#include "vertice.h"

typedef std::tuple<vertice,vertice,vertice,vertice> polygon4;
typedef std::tuple<polygon4,polygon4,polygon4,polygon4> polygon4_quadruplet;

class T_Square:public IFS{
    std::deque<polygon4> TSquare_Polygones;
    std::vector<polygon4> TSquare_PolygonesPrint;
public:
    T_Square(int iteration_limit=5,float time_limit=2.0);
    ~T_Square();
private:
    void print();
    void update();
    void find(polygon4 p,polygon4_quadruplet *pq);
};

#endif /* defined(__Fractals__T_Square__) */
