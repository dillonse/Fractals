//
//  SierpinskiCarpet.h
//  Fractals
//
//  Created by Sean Dillon on 11/6/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__SierpinskiCarpet__
#define __Fractals__SierpinskiCarpet__

#include <deque>
#include <vector>
#include <math.h>
#include "IFS.h"

#include "vertice.h"

typedef std::tuple<vertice,vertice,vertice,vertice> polygon4;
typedef std::tuple<polygon4,polygon4,polygon4,polygon4,polygon4,polygon4,polygon4,polygon4> polygon4_eightplet;

class SierpinskiCarpet:public IFS{
    std::deque<polygon4> Sierpinski_Polygones;
    std::vector<polygon4> Sierpinski_PolygonesPrint;
public:
    SierpinskiCarpet(int iteration_limit=5,float time_limit=2.0);
    ~SierpinskiCarpet();
private:
    void print();
    void update();
    void find(polygon4 p,polygon4_eightplet *pq);
};

#endif /* defined(__Fractals__SierpinskiCarpet__) */
