//
//  MengerSponge.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "MengerSponge.h"



cube operator+(cube c,vertice v){
    return cube(std::get<0>(c)+v,std::get<1>(c)+v,std::get<2>(c)+v,std::get<3>(c)+v,
                std::get<4>(c)+v,std::get<5>(c)+v,std::get<6>(c)+v,std::get<7>(c)+v);
}

cube operator-(cube c,vertice v){
    return cube(std::get<0>(c)-v,std::get<1>(c)-v,std::get<2>(c)-v,std::get<3>(c)-v,
                std::get<4>(c)-v,std::get<5>(c)-v,std::get<6>(c)-v,std::get<7>(c)-v);
}


Menger::Menger(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Menger_Cubes.push_back(cube(vertice(-1,1,-20),vertice(1,1,-20),vertice(1,-1,-20),vertice(-1,-1,-20),
                                vertice(-1,1,-22),vertice(1,1,-22),vertice(1,-1,-22),vertice(-1,-1,-22)));
    vertice &v1 = std::get<0>(Menger_Cubes[0]);
    vertice &v7 = std::get<6>(Menger_Cubes[0]);
    center = vertice((std::get<0>(v1)+std::get<0>(v7))/2,
                             (std::get<1>(v1)+std::get<1>(v7))/2,
                             (std::get<2>(v1)+std::get<2>(v7))/2);
    name = "Menger Sponge";
}

Menger::~Menger(){
    Menger_Cubes.clear();
}

void Menger::print(){
    for (int i=0;i<Menger_Cubes.size();i++){
        cube&c = Menger_Cubes[i];
        vertice&v1 = std::get<0>(c);
        vertice&v2 = std::get<1>(c);
        vertice&v3 = std::get<2>(c);
        vertice&v4 = std::get<3>(c);
        vertice&v5 = std::get<4>(c);
        vertice&v6 = std::get<5>(c);
        vertice&v7 = std::get<6>(c);
        vertice&v8 = std::get<7>(c);
        glPushMatrix();
        glTranslatef(std::get<0>(center),std::get<1>(center),std::get<2>(center));
        glRotatef((timer/time_limit)*360,1,1,0);
        glTranslatef(-std::get<0>(center),-std::get<1>(center),-std::get<2>(center));
        glBegin(GL_QUADS);
        //p1
        glColor3f(1,0,0);
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        //p2
        glVertex3f(std::get<0>(v5),std::get<1>(v5),std::get<2>(v5));
        glVertex3f(std::get<0>(v6),std::get<1>(v6),std::get<2>(v6));
        glVertex3f(std::get<0>(v7),std::get<1>(v7),std::get<2>(v7));
        glVertex3f(std::get<0>(v8),std::get<1>(v8),std::get<2>(v8));
        //p3
        glColor3f(0,1,0);
        glVertex3f(std::get<0>(v5),std::get<1>(v5),std::get<2>(v5));
        glVertex3f(std::get<0>(v6),std::get<1>(v6),std::get<2>(v6));
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        //p4
        glVertex3f(std::get<0>(v8),std::get<1>(v8),std::get<2>(v8));
        glVertex3f(std::get<0>(v7),std::get<1>(v7),std::get<2>(v7));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        //p5
        glColor3f(0,0,1);
        glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
        glVertex3f(std::get<0>(v6),std::get<1>(v6),std::get<2>(v6));
        glVertex3f(std::get<0>(v7),std::get<1>(v7),std::get<2>(v7));
        glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
        //p6
        glVertex3f(std::get<0>(v5),std::get<1>(v5),std::get<2>(v5));
        glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
        glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
        glVertex3f(std::get<0>(v8),std::get<1>(v8),std::get<2>(v8));
        
        glEnd();
        glPopMatrix();;
    }
}

void Menger::update(){
    size_t last_size = Menger_Cubes.size();
    cube_twantuple ct;
    for(int i=0;i<last_size;i++){
        find(Menger_Cubes[0],&ct);
        Menger_Cubes.push_back(std::get<0>(ct));
        Menger_Cubes.push_back(std::get<1>(ct));
        Menger_Cubes.push_back(std::get<2>(ct));
        Menger_Cubes.push_back(std::get<3>(ct));
        Menger_Cubes.push_back(std::get<4>(ct));
        Menger_Cubes.push_back(std::get<5>(ct));
        Menger_Cubes.push_back(std::get<6>(ct));
        Menger_Cubes.push_back(std::get<7>(ct));
        Menger_Cubes.push_back(std::get<8>(ct));
        Menger_Cubes.push_back(std::get<9>(ct));
        Menger_Cubes.push_back(std::get<10>(ct));
        Menger_Cubes.push_back(std::get<11>(ct));
        Menger_Cubes.push_back(std::get<12>(ct));
        Menger_Cubes.push_back(std::get<13>(ct));
        Menger_Cubes.push_back(std::get<14>(ct));
        Menger_Cubes.push_back(std::get<15>(ct));
        Menger_Cubes.push_back(std::get<16>(ct));
        Menger_Cubes.push_back(std::get<17>(ct));
        Menger_Cubes.push_back(std::get<18>(ct));
        Menger_Cubes.push_back(std::get<19>(ct));
        Menger_Cubes.pop_front();
    }
    
}
void Menger::find(cube &c,cube_twantuple *ct){
    //get the cube vertices//
    vertice&v1 = std::get<0>(c);
    vertice&v2 = std::get<1>(c);
    vertice&v3 = std::get<2>(c);
    vertice&v5 = std::get<4>(c);
    //find the offsets
    vertice offset1 = (v2-v1)/3;
    vertice offset2 = (v3-v2)/3;
    vertice offset3 = (v5-v1)/3;
    
    //f1
    cube c11 = cube(v1,v1+offset1,v1+offset1+offset2,v1+offset2,
                    v1+offset3,v1+offset1+offset3,v1+offset1+offset2+offset3,v1+offset2+offset3);
    cube c12 = c11+offset1;
    
    cube c13 = c12+offset1;
    
    cube c14 = c13+offset2;
    
    cube c15 = c14+offset2;
    
    cube c16 = c15-offset1;
    
    cube c17 = c16-offset1;
    
    cube c18 = c17-offset2;
    //f2
    cube c21 = c11+offset3*2;
    cube c22 = c11+offset3;
    cube c26 = c17+offset3;
    cube c27 = c26+offset3;
    cube c28 = c27-offset2;
    
    //f3
    cube c31 = c21+offset1*2;
    cube c32 = c22+offset1*2;
    cube c36 = c26+offset1*2;
    cube c37 = c27+offset1*2;
    cube c38 = c28+offset1*2;
    //f4
    cube c42 = c12+offset3*2;
    cube c46 = c16+offset3*2;
    
    *ct = cube_twantuple(c11,c12,c13,c14,c15,c16,c17,c18,c21,c22,c26,c27,c28,c31,c32,c36,c37,c38,c42,c46);

}







