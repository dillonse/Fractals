//
//  IFS.h
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#ifndef __Fractals__IFS__
#define __Fractals__IFS__
#include <iostream>

class IFS{
protected:
    float timer;
    float speed;
    int iteration_limit;
    int time_limit;
    int iteration_counter=0;
    std::string name;
public:
    IFS(int iteration_limit=5,float time_limit=2.0);
    void animate();
    std::string getName(){return name;}
    void increment_iterations(){iteration_limit++;}
private:
    virtual void print(){};
    virtual void update(){};
    virtual void find(){};
};

#endif /* defined(__Fractals__IFS__) */
