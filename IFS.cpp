//
//  IFS.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "IFS.h"



IFS::IFS(int iteration_limit,float time_limit){
    timer=0.0;
    speed=0.01;
    this->iteration_limit=iteration_limit;
    this->time_limit=time_limit;
}

void IFS::animate(){
    if(timer>=time_limit){
        timer=0;
        if(iteration_counter<iteration_limit){
            update();
            iteration_counter++;
        }
    }
    else{
        timer+=speed;
    }
    print();
}
