//
//  SierpinskiCarpet.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/6/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "SierpinskiCarpet.h"

SierpinskiCarpet::SierpinskiCarpet(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Sierpinski_Polygones.push_back(polygon4(vertice(-0.2,0.2,-10),vertice(0.2,0.2,-10),vertice(0.2,-0.2,-10),vertice(-0.2,-0.2,-10)));
    name = "Sierpinski Carpet";
}

SierpinskiCarpet::~SierpinskiCarpet(){
    Sierpinski_Polygones.clear();
    Sierpinski_PolygonesPrint.clear();
}

void SierpinskiCarpet::print(){
        glColor3f(0.5,0.5,0.9);
        for (int i=0;i<Sierpinski_Polygones.size();i++){
            glBegin(GL_POLYGON);
            polygon4 &p = Sierpinski_Polygones[i];
            vertice &v1 = std::get<0>(p);
            vertice &v2 = std::get<1>(p);
            vertice &v3 = std::get<2>(p);
            vertice &v4 = std::get<3>(p);
            glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
            glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
            glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
            glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
            glEnd();
        }
        for(int i=0;i<Sierpinski_PolygonesPrint.size();i++){
            glBegin(GL_QUADS);
            polygon4 &p = Sierpinski_PolygonesPrint[i];
            vertice &v1 = std::get<0>(p);
            vertice &v2 = std::get<1>(p);
            vertice &v3 = std::get<2>(p);
            vertice &v4 = std::get<3>(p);
            glVertex3f(std::get<0>(v1),std::get<1>(v1),std::get<2>(v1));
            glVertex3f(std::get<0>(v2),std::get<1>(v2),std::get<2>(v2));
            glVertex3f(std::get<0>(v3),std::get<1>(v3),std::get<2>(v3));
            glVertex3f(std::get<0>(v4),std::get<1>(v4),std::get<2>(v4));
            glEnd();
        }
}

void SierpinskiCarpet::update(){
    polygon4_eightplet pq;
    size_t last_size= Sierpinski_Polygones.size();
    for(int i=0;i<last_size;i++){
        polygon4&p = Sierpinski_Polygones[0];
        find(p,&pq);
        Sierpinski_Polygones.push_back(std::get<0>(pq));
        Sierpinski_Polygones.push_back(std::get<1>(pq));
        Sierpinski_Polygones.push_back(std::get<2>(pq));
        Sierpinski_Polygones.push_back(std::get<3>(pq));
        Sierpinski_Polygones.push_back(std::get<4>(pq));
        Sierpinski_Polygones.push_back(std::get<5>(pq));
        Sierpinski_Polygones.push_back(std::get<6>(pq));
        Sierpinski_Polygones.push_back(std::get<7>(pq));
        Sierpinski_Polygones.pop_front();
        Sierpinski_PolygonesPrint.push_back(p);
    }
}

void SierpinskiCarpet::find(polygon4 p,polygon4_eightplet* ps){
    vertice&v1 = std::get<0>(p);
    vertice&v2 = std::get<1>(p);
    vertice&v3 = std::get<2>(p);
    vertice&v4 = std::get<3>(p);
    //find the midpoints
    
    vertice mp11 = vertice(std::get<0>(v1)+(std::get<0>(v2)-std::get<0>(v1))/3,
                          std::get<1>(v1)+(std::get<1>(v2)-std::get<1>(v1))/3,
                          std::get<2>(v1)+(std::get<2>(v2)-std::get<2>(v1))/3);
    
    vertice mp12 = vertice(std::get<0>(v2)+(std::get<0>(v1)-std::get<0>(v2))/3,
                          std::get<1>(v2)+(std::get<1>(v1)-std::get<1>(v2))/3,
                          std::get<2>(v2)+(std::get<2>(v1)-std::get<2>(v2))/3);
    
    vertice mp21 = vertice(std::get<0>(v2)+(std::get<0>(v3)-std::get<0>(v2))/3,
                           std::get<1>(v2)+(std::get<1>(v3)-std::get<1>(v2))/3,
                           std::get<2>(v2)+(std::get<2>(v3)-std::get<2>(v2))/3);
    
    vertice mp22 = vertice(std::get<0>(v3)+(std::get<0>(v2)-std::get<0>(v3))/3,
                           std::get<1>(v3)+(std::get<1>(v2)-std::get<1>(v3))/3,
                           std::get<2>(v3)+(std::get<2>(v2)-std::get<2>(v3))/3);
    
    
    vertice mp31 = vertice(std::get<0>(v3)+(std::get<0>(v4)-std::get<0>(v3))/3,
                           std::get<1>(v3)+(std::get<1>(v4)-std::get<1>(v3))/3,
                           std::get<2>(v3)+(std::get<2>(v4)-std::get<2>(v3))/3);
    
    vertice mp32 = vertice(std::get<0>(v4)+(std::get<0>(v3)-std::get<0>(v4))/3,
                           std::get<1>(v4)+(std::get<1>(v3)-std::get<1>(v4))/3,
                           std::get<2>(v4)+(std::get<2>(v3)-std::get<2>(v4))/3);

    vertice mp41 = vertice(std::get<0>(v4)+(std::get<0>(v1)-std::get<0>(v4))/3,
                           std::get<1>(v4)+(std::get<1>(v1)-std::get<1>(v4))/3,
                           std::get<2>(v4)+(std::get<2>(v1)-std::get<2>(v4))/3);
    
    vertice mp42 = vertice(std::get<0>(v1)+(std::get<0>(v4)-std::get<0>(v1))/3,
                           std::get<1>(v1)+(std::get<1>(v4)-std::get<1>(v1))/3,
                           std::get<2>(v1)+(std::get<2>(v4)-std::get<2>(v1))/3);
    //find the offset to vertical and horizontal movement//
    vertice offset1 = vertice(std::get<0>(mp11)-std::get<0>(v1),std::get<1>(mp11)-std::get<1>(v1),std::get<2>(mp11)-std::get<2>(v1));
    vertice offset2 = vertice(std::get<0>(mp21)-std::get<0>(v2),std::get<1>(mp21)-std::get<1>(v2),std::get<2>(mp21)-std::get<2>(v2));
    //create the polygons//
    polygon4 p1=polygon4(v1,mp11,
                         vertice(std::get<0>(mp11)+std::get<0>(offset2),
                                 std::get<1>(mp11)+std::get<1>(offset2),
                                 std::get<2>(mp11)+std::get<2>(offset2)),
                         mp42);
    polygon4 p2=polygon4(mp11,mp12,
                         vertice(std::get<0>(mp12)+std::get<0>(offset2),
                                 std::get<1>(mp12)+std::get<1>(offset2),
                                 std::get<2>(mp12)+std::get<2>(offset2)),
                         vertice(std::get<0>(mp11)+std::get<0>(offset2),
                                 std::get<1>(mp11)+std::get<1>(offset2),
                                 std::get<2>(mp11)+std::get<2>(offset2)));
    polygon4 p3=polygon4(mp12,v2,mp21,
                         vertice(std::get<0>(mp21)-std::get<0>(offset1),
                                 std::get<1>(mp21)-std::get<1>(offset1),
                                 std::get<2>(mp21)-std::get<2>(offset1)));
    
    polygon4 p4=polygon4(vertice(std::get<0>(mp21)-std::get<0>(offset1),
                                 std::get<1>(mp21)-std::get<1>(offset1),
                                 std::get<2>(mp21)-std::get<2>(offset1)),mp21,mp22,
                         vertice(std::get<0>(mp22)-std::get<0>(offset1),
                                 std::get<1>(mp22)-std::get<1>(offset1),
                                 std::get<2>(mp22)-std::get<2>(offset1)));
    polygon4 p5=polygon4(vertice(std::get<0>(mp22)-std::get<0>(offset1),
                                 std::get<1>(mp22)-std::get<1>(offset1),
                                 std::get<2>(mp22)-std::get<2>(offset1)),mp22,v3,mp31);
    polygon4 p6=polygon4(vertice(std::get<0>(mp32)-std::get<0>(offset2),
                                 std::get<1>(mp32)-std::get<1>(offset2),
                                 std::get<2>(mp32)-std::get<2>(offset2)),
                         vertice(std::get<0>(mp22)-std::get<0>(offset1),
                                 std::get<1>(mp22)-std::get<1>(offset1),
                                 std::get<2>(mp22)-std::get<2>(offset1)),
                         mp31,mp32);
    polygon4 p7=polygon4(mp41,vertice(std::get<0>(mp32)-std::get<0>(offset2),
                                       std::get<1>(mp32)-std::get<1>(offset2),
                                       std::get<2>(mp32)-std::get<2>(offset2)),
                               mp32,v4);
    polygon4 p8=polygon4(mp42,
                         vertice(std::get<0>(mp11)+std::get<0>(offset2),
                                 std::get<1>(mp11)+std::get<1>(offset2),
                                 std::get<2>(mp11)+std::get<2>(offset2)),
                         vertice(std::get<0>(mp32)-std::get<0>(offset2),
                                 std::get<1>(mp32)-std::get<1>(offset2),
                                 std::get<2>(mp32)-std::get<2>(offset2)),mp41);
    //move the polygons away from the center//
    //p1
    std::get<0>(std::get<0>(p1))+= (-std::get<0>(offset2) -std::get<0>(offset1))*2;
    std::get<1>(std::get<0>(p1))+= (-std::get<1>(offset2) -std::get<1>(offset1))*2;
    
    std::get<0>(std::get<1>(p1))+= (-std::get<0>(offset2) -std::get<0>(offset1))*2;
    std::get<1>(std::get<1>(p1))+= (-std::get<1>(offset2) -std::get<1>(offset1))*2;
    
    std::get<0>(std::get<2>(p1))+= (-std::get<0>(offset2) -std::get<0>(offset1))*2;
    std::get<1>(std::get<2>(p1))+= (-std::get<1>(offset2) -std::get<1>(offset1))*2;
    
    std::get<0>(std::get<3>(p1))+= (-std::get<0>(offset2)-std::get<0>(offset1))*2;
    std::get<1>(std::get<3>(p1))+= (-std::get<1>(offset2)-std::get<1>(offset1))*2;
    
    //p2
    std::get<0>(std::get<0>(p2))+= (-std::get<0>(offset2))*2;
    std::get<1>(std::get<0>(p2))+= (-std::get<1>(offset2))*2;
    
    std::get<0>(std::get<1>(p2))+= (-std::get<0>(offset2))*2;
    std::get<1>(std::get<1>(p2))+= (-std::get<1>(offset2))*2;
    
    std::get<0>(std::get<2>(p2))+= (-std::get<0>(offset2))*2;
    std::get<1>(std::get<2>(p2))+= (-std::get<1>(offset2))*2;
    
    std::get<0>(std::get<3>(p2))+= (-std::get<0>(offset2))*2;
    std::get<1>(std::get<3>(p2))+= (-std::get<1>(offset2))*2;
    
    //p3
    std::get<0>(std::get<0>(p3))+= (-std::get<0>(offset2) +std::get<0>(offset1))*2;
    std::get<1>(std::get<0>(p3))+= (-std::get<1>(offset2) +std::get<1>(offset1))*2;
    
    std::get<0>(std::get<1>(p3))+= (-std::get<0>(offset2) +std::get<0>(offset1))*2;
    std::get<1>(std::get<1>(p3))+= (-std::get<1>(offset2) +std::get<1>(offset1))*2;
    
    std::get<0>(std::get<2>(p3))+= (-std::get<0>(offset2) +std::get<0>(offset1))*2;
    std::get<1>(std::get<2>(p3))+= (-std::get<1>(offset2) +std::get<1>(offset1))*2;
    
    std::get<0>(std::get<3>(p3))+= (-std::get<0>(offset2)+std::get<0>(offset1))*2;
    std::get<1>(std::get<3>(p3))+= (-std::get<1>(offset2)+std::get<1>(offset1))*2;

    //p4
    std::get<0>(std::get<0>(p4))+= (std::get<0>(offset1))*2;
    std::get<1>(std::get<0>(p4))+= (std::get<1>(offset1))*2;
    
    std::get<0>(std::get<1>(p4))+= (std::get<0>(offset1))*2;
    std::get<1>(std::get<1>(p4))+= (std::get<1>(offset1))*2;
    
    std::get<0>(std::get<2>(p4))+= (std::get<0>(offset1))*2;
    std::get<1>(std::get<2>(p4))+= (std::get<1>(offset1))*2;
    
    std::get<0>(std::get<3>(p4))+= (std::get<0>(offset1))*2;
    std::get<1>(std::get<3>(p4))+= (std::get<1>(offset1))*2;
    
    //p5
    std::get<0>(std::get<0>(p5))+= (std::get<0>(offset2) +std::get<0>(offset1))*2;
    std::get<1>(std::get<0>(p5))+= (std::get<1>(offset2) +std::get<1>(offset1))*2;
    
    std::get<0>(std::get<1>(p5))+= (std::get<0>(offset2) +std::get<0>(offset1))*2;
    std::get<1>(std::get<1>(p5))+= (std::get<1>(offset2) +std::get<1>(offset1))*2;
    
    std::get<0>(std::get<2>(p5))+= (std::get<0>(offset2) +std::get<0>(offset1))*2;
    std::get<1>(std::get<2>(p5))+= (std::get<1>(offset2) +std::get<1>(offset1))*2;
    
    std::get<0>(std::get<3>(p5))+= (std::get<0>(offset2)+std::get<0>(offset1))*2;
    std::get<1>(std::get<3>(p5))+= (std::get<1>(offset2)+std::get<1>(offset1))*2;

    //p6
    std::get<0>(std::get<0>(p6))+= (std::get<0>(offset2))*2;
    std::get<1>(std::get<0>(p6))+= (std::get<1>(offset2))*2;
    
    std::get<0>(std::get<1>(p6))+= (std::get<0>(offset2))*2;
    std::get<1>(std::get<1>(p6))+= (std::get<1>(offset2))*2;
    
    std::get<0>(std::get<2>(p6))+= (std::get<0>(offset2))*2;
    std::get<1>(std::get<2>(p6))+= (std::get<1>(offset2))*2;
    
    std::get<0>(std::get<3>(p6))+= (std::get<0>(offset2))*2;
    std::get<1>(std::get<3>(p6))+= (std::get<1>(offset2))*2;

    //p7
    std::get<0>(std::get<0>(p7))+= (std::get<0>(offset2) -std::get<0>(offset1))*2;
    std::get<1>(std::get<0>(p7))+= (std::get<1>(offset2) -std::get<1>(offset1))*2;
    
    std::get<0>(std::get<1>(p7))+= (std::get<0>(offset2) -std::get<0>(offset1))*2;
    std::get<1>(std::get<1>(p7))+= (std::get<1>(offset2) -std::get<1>(offset1))*2;
    
    std::get<0>(std::get<2>(p7))+= (std::get<0>(offset2) -std::get<0>(offset1))*2;
    std::get<1>(std::get<2>(p7))+= (std::get<1>(offset2) -std::get<1>(offset1))*2;
    
    std::get<0>(std::get<3>(p7))+= (std::get<0>(offset2)-std::get<0>(offset1))*2;
    std::get<1>(std::get<3>(p7))+= (std::get<1>(offset2)-std::get<1>(offset1))*2;
    
    //p8
    std::get<0>(std::get<0>(p8))+= (-std::get<0>(offset1))*2;
    std::get<1>(std::get<0>(p8))+= (-std::get<1>(offset1))*2;
    
    std::get<0>(std::get<1>(p8))+= (-std::get<0>(offset1))*2;
    std::get<1>(std::get<1>(p8))+= (-std::get<1>(offset1))*2;
    
    std::get<0>(std::get<2>(p8))+= (-std::get<0>(offset1))*2;
    std::get<1>(std::get<2>(p8))+= (-std::get<1>(offset1))*2;
    
    std::get<0>(std::get<3>(p8))+= (-std::get<0>(offset1))*2;
    std::get<1>(std::get<3>(p8))+= (-std::get<1>(offset1))*2;
    
    
    *ps = polygon4_eightplet(p1,p2,p3,p4,p5,p6,p7,p8);
}