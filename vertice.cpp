//
//  vertice.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "vertice.h"
vertice operator+(vertice v1,vertice v2){
    return vertice(std::get<0>(v1)+std::get<0>(v2),
                   std::get<1>(v1)+std::get<1>(v2),
                   std::get<2>(v1)+std::get<2>(v2));
}

vertice operator-(vertice v1,vertice v2){
    return vertice(std::get<0>(v1)-std::get<0>(v2),
                   std::get<1>(v1)-std::get<1>(v2),
                   std::get<2>(v1)-std::get<2>(v2));
}

vertice operator/(vertice v1,int i){
    return vertice(std::get<0>(v1)/i,std::get<1>(v1)/i,std::get<2>(v1)/i);
}

vertice operator*(vertice v1,int i){
    return vertice(std::get<0>(v1)*i,std::get<1>(v1)*i,std::get<2>(v1)*i);
}