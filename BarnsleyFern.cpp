//
//  BarnsleyFern.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/7/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "BarnsleyFern.h"
#include <stdlib.h>
#include <iostream>

Barnsley::Barnsley(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Barnsley_Points.push_back(vertice(+1,-1,-100));
    name = "Barsley Fern";
}

Barnsley::~Barnsley(){
    Barnsley_Points.clear();
}

void Barnsley::print(){
    glPointSize(1);
    glColor3f(0,0.33,0);
    glBegin(GL_POINTS);
    for(int i=0;i<Barnsley_Points.size();i++){
        glVertex3f(std::get<0>(Barnsley_Points[i]),std::get<1>(Barnsley_Points[i]),std::get<2>(Barnsley_Points[i]));
    }
    glEnd();
}

void Barnsley::update(){
    vertice v2;
    size_t last_size = Barnsley_Points.size();
    for(int i=0;i<last_size;i++){
        vertice&v1=Barnsley_Points[i];
        find(v1,&v2);
        Barnsley_Points.push_back(v2);
    }
    std::cout<<"Num of points "<<Barnsley_Points.size()<<std::endl;
}

void Barnsley::find(vertice &v1,vertice *v2){
    float random_choice = random()%100;
    if(random_choice>15){//p = 85%
        *v2 = vertice(std::get<0>(v1)*0.85+std::get<1>(v1)*0.04,std::get<0>(v1)*(-0.04)+std::get<1>(v1)*0.85+1.6,
                      std::get<2>(v1));
    }
    else if(random_choice>8){//p=7%
        *v2 = vertice(std::get<0>(v1)*0.2+std::get<1>(v1)*(-0.26),std::get<0>(v1)*(0.23)+std::get<1>(v1)*0.22+1.6,
                      std::get<2>(v1));
    }
    else if(random_choice>1){//p=7%
                *v2 = vertice(std::get<0>(v1)*(-0.15)+std::get<1>(v1)*(0.28),std::get<0>(v1)*(0.26)+std::get<1>(v1)*0.24+1.6,std::get<2>(v1));
    }
    else{//p=1%
        *v2 = vertice(0,0.16*std::get<1>(v1),std::get<2>(v1));
    }
}