//
//  SierpinskiTriangle.cpp
//  Fractals
//
//  Created by Sean Dillon on 11/5/14.
//  Copyright (c) 2014 Sean Dillon. All rights reserved.
//

#include "SierpinskiTriangle.h"


Sierpinski::Sierpinski(int iteration_limit,float time_limit):IFS(iteration_limit,time_limit){
    Sierpinski_Triangles.push_back(triangle(vertice(0.0,0.7,-10),vertice(-1.0,-1.0,-10),vertice(1.0,-1.0,-10)));
    glColor3f(0.3,0.5,0.2);
    name = "Sierpinski Triangle";
    
}

Sierpinski::~Sierpinski(){
            Sierpinski_Triangles.clear();
}


void Sierpinski::print(){
    glBegin(GL_TRIANGLES);
    for(int i=0;i<Sierpinski_Triangles.size();i++){
        triangle& t = Sierpinski_Triangles[i];
        vertice& v1=std::get<0>(t);
        vertice& v2=std::get<1>(t);
        vertice& v3=std::get<2>(t);
        glVertex3f(std::get<0>(v1),std::get<1>(v1), std::get<2>(v1));
        glVertex3f(std::get<0>(v2),std::get<1>(v2), std::get<2>(v2));
        glVertex3f(std::get<0>(v3),std::get<1>(v3), std::get<2>(v3));
    }
    glEnd();
    
}

void Sierpinski::update(){
    triangle_triplet tt;
    size_t last_size = Sierpinski_Triangles.size();
    for(int i=0;i<last_size;i++){
        triangle& t = Sierpinski_Triangles[0];
        //find next here and push them to deque//
        find(t,&tt);
        //push the triplet at the back of the deque//
        Sierpinski_Triangles.push_back(std::get<0>(tt));
        Sierpinski_Triangles.push_back(std::get<1>(tt));
        Sierpinski_Triangles.push_back(std::get<2>(tt));
        Sierpinski_Triangles.pop_front();
    }
}

void Sierpinski::find(triangle t,triangle_triplet *tt){
    //find the midpoint for each of the edges//
    vertice& v1 = std::get<0>(t);
    vertice& v2 = std::get<1>(t);
    vertice& v3 = std::get<2>(t);
    vertice mp1 = vertice((std::get<0>(v1)+std::get<0>(v2))/2,(std ::get<1>(v1)+std::get<1>(v2))/2,(std::get<2>(v1)+std::get<2>(v2))/2);
    vertice mp2 = vertice((std::get<0>(v2)+std::get<0>(v3))/2,(std::get<1>(v2)+std::get<1>(v3))/2,(std::get<2>(v2)+std::get<2>(v3))/2);
    vertice mp3 = vertice((std::get<0>(v3)+std::get<0>(v1))/2,(std::get<1>(v3)+std::get<1>(v1))/2,(std::get<2>(v3)+std::get<2>(v1))/2);
    //create the three triangles from the midpoints and the original points//
    *tt = triangle_triplet(triangle(mp1,v2,mp2),triangle(mp2,v3,mp3),triangle(mp3,v1,mp1));
}
